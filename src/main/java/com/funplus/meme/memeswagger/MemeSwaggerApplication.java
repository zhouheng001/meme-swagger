package com.funplus.meme.memeswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemeSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemeSwaggerApplication.class, args);
    }

}

package com.funplus.meme.memeswagger.config;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * @author heng.zhou
 * @description: swagger配置类
 * @create 2019-12-27 3:18 下午
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Value("${meme.domain}")
    private String memeDomainUrl;


    @Autowired
    private TypeResolver typeResolver;


    @Bean
    public Docket memeActivity() {

        return new Docket(DocumentationType.SWAGGER_2).groupName("meme-activity").select()
                .apis(RequestHandlerSelectors.basePackage("com.funplus.meme.memeswagger.controller.activity")).paths(PathSelectors.any())
                .build().pathMapping("/").genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(true).forCodeGeneration(false)
                .host(memeDomainUrl)
//                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo("meme_activity", "文档中可以查询及测试接口调用参数和结果", "1.0"));
    }


    @Bean
    public Docket memeRanks() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("meme-h5").select()
                .apis(RequestHandlerSelectors.basePackage("com.funplus.meme.memeswagger.controller.ranks"))
                .paths(PathSelectors.any()).build().pathMapping("/").genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(true).forCodeGeneration(false)
                .host(memeDomainUrl)
//                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo("meme-h5", "文档中可以查询及h5测试接口调用参数和结果", "1.0"));
    }

    @Bean
    public Docket memeEvent() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("meme-event").select()
                .apis(RequestHandlerSelectors.basePackage("com.funplus.meme.memeswagger.controller.event"))
                .paths(PathSelectors.any()).build().pathMapping("/").genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(true).forCodeGeneration(false)
                .host(memeDomainUrl)
//                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo("meme-event", "文档中可以查询及测试接口调用参数和结果", "1.0"));
    }

    @Bean
    public Docket memeLatestRank() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("meme-latestrank").select()
                .apis(RequestHandlerSelectors.basePackage("com.funplus.meme.memeswagger.controller.lastrank"))
                .paths(PathSelectors.any()).build().pathMapping("/").genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(true).forCodeGeneration(false)
                .host(memeDomainUrl)
//                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo("meme-latestrank", "文档中可以查询及测试接口调用参数和结果", "1.0"));
    }

    @Bean
    public Docket memeBaseService() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("meme-base-service").select()
                .apis(RequestHandlerSelectors.basePackage("com.funplus.meme.memeswagger.controller.baseservice"))
                .paths(PathSelectors.any()).build().pathMapping("/").genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(
                        typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                        typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(true).forCodeGeneration(false)
                .host(memeDomainUrl)
//                .globalOperationParameters(getParameters())
                .apiInfo(apiInfo("meme-base-service", "文档中可以查询及测试接口调用参数和结果", "1.0"));
    }

    private ApiInfo apiInfo(String title, String description, String version) {
        return new ApiInfoBuilder().title(title).description(description).contact("heng.zhou").version(version).build();

    }

    private List<Parameter> getParameters() {

        ParameterBuilder parameterBuilder = new ParameterBuilder();
        List<Parameter> parameters = new ArrayList<Parameter>();
        parameterBuilder.name("X-Authorization")
                .description("token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false).build();
        parameters.add(parameterBuilder.build());
        return parameters;

    }
}

package com.funplus.meme.memeswagger.model;

/**
 * Created by funplus on 2017/1/16.
 */
public class GiftRank{
    public long giftid;
    public int rank;
    public String giftname;
    public String giftpic;
    public long num;
    public int diamond;
    public long total;
}
